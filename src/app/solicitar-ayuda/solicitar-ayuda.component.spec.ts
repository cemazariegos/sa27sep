import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { SolicitarAyudaComponent } from './solicitar-ayuda.component';
import {HttpClient} from '@angular/common/http';
import {LoginService} from '../services/login.service';
import {SolicitarAyudaService} from '../services/solicitar-ayuda.service'
import {Router} from '@angular/router';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('SolicitarAyudaComponent', () => {
  let component: SolicitarAyudaComponent;
  let fixture: ComponentFixture<SolicitarAyudaComponent>;
  let AyudaService;
  let ayuda_egistro;
  let http;
  let app: SolicitarAyudaComponent;
  let router;

  
       
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolicitarAyudaComponent ],
      imports:[HttpClientTestingModule,FormsModule],
      providers:[LoginService],
      
     
    })
    .compileComponents();
  }));


  beforeEach(() => {
   fixture = TestBed.createComponent(SolicitarAyudaComponent);
   component = fixture.componentInstance;
   fixture.detectChanges();
   http=HttpClient;
   AyudaService=  LoginService;
   ayuda_egistro= SolicitarAyudaService;
   router=Router;
   app=new SolicitarAyudaComponent(AyudaService,ayuda_egistro);
  });

  it('La pagina debería de cargar correctamente, el formulario debe ser visible', () => {
    expect(component).toBeTruthy();
  });

  it('debe tomar los datos de nombre',async(()=>{
    (<HTMLInputElement>document.getElementById('nombre')).value='marcos';
    (<HTMLInputElement>document.getElementById('apellido')).value='santos';
    component.info.nombre="marcos";
    expect(component.info.nombre).toEqual("marcos");

  }))

  it('Se ejecuta el metodo de enviar datos',async(()=>{
  
    component.info.nombre="marcos";
    component.info.apellido="santos"
    expect(component.enviar_datos()).toBe(true);

  }))

  it('rechaza de no tener todos los campos llenos',async(()=>{
  
    component.info.nombre="";
    component.info.apellido=""
    expect(component.enviar_datos()).toBe(false);

  }))
});


