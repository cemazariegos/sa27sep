import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms'
import { AppRoutingModule } from '../app-routing.module';
import { GraphQLModule } from '../graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { MarkdownModule } from "ngx-markdown";
import { NavComponent } from "../nav/nav.component";
import {CommonModule} from '@angular/common';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { LoginComponent } from '../login/login.component';
import { RegistroComponent } from '../registro/registro.component';
import { VistaListadoClientesComponent } from '../vista-listado-clientes/vista-listado-clientes.component';

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports:[
        FormsModule,
        MarkdownModule.forRoot(),
        BrowserModule,
        AppRoutingModule,
        GraphQLModule,
        HttpClientModule,
        CommonModule,
        NgbModule,
        NgMultiSelectDropDownModule.forRoot()
      ],
      declarations: [ NavComponent,
        LoginComponent,
        RegistroComponent,
        VistaListadoClientesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
