import { Component, OnInit } from '@angular/core';
import { Apollo } from "apollo-angular";
import gql from "graphql-tag";
import CATEGORIES_QUERY from "../apollo/queries/category/categories";
import { Subscription } from "rxjs";
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { SolicitudService} from '../services/solicitud.service'
import { CategoriaService } from '../services/categoria.service'
import { Categoria } from '../models/categoria.interface'
import { LoginService }  from '../services/login.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {
  data: any = {};
  loading = true;
  errors: any;
  closeResult: string;
  dropdownList = [];
  selectedItems = [];
  dropdownSettings = {};
  solicitud={
    titulo:'',
    descripcion:'',
    direccion_servicio:'',
    categorias:[]
  };
  usuario:{};
  
  private queryCategories: Subscription;

  constructor(private apollo: Apollo, private modalService: NgbModal,public solicitudService: SolicitudService,public categoria:CategoriaService,public login:LoginService) {}

  getSetCategorias(){
    this.dropdownSettings = {
      singleSelection: false,
      idField: 'item_id',
      textField: 'item_text',
      selectAllText: 'Seleccionar todos',
      unSelectAllText: 'Deseleccionar todos',
      itemsShowLimit: 3,
      allowSearchFilter: true
    };

    this.categoria.get(null,{}).subscribe((res:Categoria[])=>{
      res.map((obj)=>{
        this.dropdownList.push({item_id:obj.id,item_text:obj.nombre});
      })        
    })
  }

  ngOnInit() {
    this.queryCategories = this.apollo
      .watchQuery({
        query: CATEGORIES_QUERY
      })
      .valueChanges.subscribe(result => {
        this.data = result.data;
        this.loading = result.loading;
        this.errors = result.errors;
      });
    this.getSetCategorias();
    this.usuario = this.login.getUseraux() || {};
  }
  ngOnDestroy() {
    this.queryCategories.unsubscribe();
  }

  onItemSelect(item: any) {
    console.log(item);
  }
  onSelectAll(items: any) {
    console.log(items);
  }

  saveSolicitud(): void{
      let categorias:Number[] = this.solicitud.categorias.map((cat)=>cat.item_id);

      this.solicitudService.post(this.solicitud.titulo, this.solicitud.descripcion, this.solicitud.direccion_servicio, categorias).subscribe((res:SolicitudService)=>{
          console.log("hola", res)  
        },
        error=>{      
          console.log("error")
        });          
        console.log("termino")
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      this.saveSolicitud()
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
