import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import {ProveedorService} from '../services/proveedor.service';
@Component({
  selector: 'app-enviar-cotizacion',
  templateUrl: './enviar-cotizacion.component.html',
  styleUrls: ['./enviar-cotizacion.component.css']
})
export class EnviarCotizacionComponent implements OnInit {
  user = {};
  constructor(private router: Router,private activateRoute:ActivatedRoute,private proveedorService:ProveedorService) { }

  ngOnInit() {
    this.getSolicitud();
  }
  onRegistrar(form) {
    if(form.value.comentario==undefined || form.value.precio==undefined  || form.value.fecha==undefined){
      alert('Faltan campos.')
      return false;
    }
    form.value.solicitud=this.solicitud;
    form.value.estado='espera';
    console.log('*******',form.value)
    this.proveedorService.setCotizacion(form.value).subscribe(
      (res) => {
        console.log("registrado", res);
        alert("¡Cotización enviada!");
        this.router.navigateByUrl("/listado-proveedor");
      },
      (err) => {
        console.error();
        alert("Error al enviar cotización.");
      }
    );

    return true;
  }
  solicitud:any=[];
  getSolicitud(){
    
    this.activateRoute.paramMap.subscribe(params  =>{
      const id = params.get('id');
      console.log('ID ',id )
      this.proveedorService.buscarUno(id).subscribe(
        res=>{          
          this.solicitud=res;
          console.log('solicitud: ',this.solicitud.titulo);
        },
        err=>{
          console.log(err);
          alert(err.message);
        }
      );
    });
  }



}
