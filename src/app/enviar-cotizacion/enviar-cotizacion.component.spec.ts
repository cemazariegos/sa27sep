import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {Router,ActivatedRoute} from '@angular/router';
import { EnviarCotizacionComponent } from './enviar-cotizacion.component';
import {ProveedorService} from '../services/proveedor.service';
describe('EnviarCotizacionComponent', () => {
  let app: EnviarCotizacionComponent;
  let router: Router;
  let act:ActivatedRoute;
  let pro:ProveedorService;
  beforeEach(async(() => {
    app=new EnviarCotizacionComponent(router,act,pro);
  }));

  it('Preuba 1 correcta para campos del registro',async(() => {
    let form={
      value:{comentario:'comentario para test'}
    }
    expect(app.onRegistrar(form)).toEqual(false);
  }));
  it('Preuba 2 correcta para campos del registro',async(() => {
    let form={
      value:{
        precio:1000.00,
        comentario:'comentario para test'
      }
    }
    expect(app.onRegistrar(form)).toEqual(true);
  }));

  //prueba para saber si trajo la solicitud
  it('Se ingreso bien la solicitud',async(() => {
    let solic={
        titulo:'test',
        descripcion:'comentario para test'
      };    
    app.solicitud=solic;
    expect(app.solicitud.titulo).toContain('test');
  }));
  
});
