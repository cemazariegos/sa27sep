import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import {login} from '../models/login.interface'
import {map} from 'rxjs/operators'
import { isNullOrUndefined } from 'util';


@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http:HttpClient) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })

 
  setUser(user:login) {
    let userString=JSON.stringify(user);
    localStorage.setItem("usuarioLogeado",userString);
    //this.cookies.set("token", token);
  }


  getUseraux() {
    let usuario=localStorage.getItem('usuarioLogeado');
    if(!isNullOrUndefined(usuario))
    {
        let user_JSON=JSON.parse(usuario);
       // console.log(user_JSON);
        return user_JSON;
    }else
    {
      return null;


    }
  }


  //get users
  getUser(){
    const url = "http://localhost:1337/users";
    return this.http.get(url);
  }

  postLogin(correo:string, contrasena:string){
    const url = "http://localhost:1337/auth/local";
   // return this.http.post(url,{  "identifier": correo, "password": contrasena},{headers:this.headers}).pipe(map(data=>data));
   return this.http.post(url,{  "identifier": correo, "password": contrasena},{headers:this.headers});
  }

  getCotizacion(correo:String){
    const url = "http://localhost:1337/cotizacions/"+correo;
    return this.http.get(url);
  }

  // METODO POST PARA REGISTRAR UN NUEVO USUARIO
  postResgistro(name:string, lastname:string, username:string, numberPhone:number,
                direction:string, email:string, password:string, tipo:string, ruta:string){
    const url = "http://localhost:1337/auth/local/register";

    // SI ES UN PROVEEDOR ENVIO EL CURRICULUM
    if(tipo === 'proveedor'){
      return this.http.post(url,{"nombre":name, "apellido":lastname, "username":username,
                                "email":email, "direccion":direction, "telefono":numberPhone,
                                "password":password, "tipo":tipo,"documento":ruta} );
    }
    
    return this.http.post(url,{"nombre":name, "apellido":lastname, "username":username,
                                "email":email, "direccion":direction, "telefono":numberPhone,
                                "password":password, "tipo":tipo} );
  }



  

}
