import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import { LoginService }  from './login.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http:HttpClient,public login:LoginService) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })
  // METODO POST PARA CREAR SOLICITUD DE SERVICIO
  resetPassword(token:string, password:string, confirm:string){
    const url = `http://localhost:1337/auth/reset-password`;
    return this.http.post(url, {code:token,password:password,passwordConfirmation:confirm},{headers:{}});
  }
}