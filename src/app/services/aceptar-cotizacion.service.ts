import { Injectable } from '@angular/core';

import { HttpClient, HttpHeaders} from '@angular/common/http'
import {cotizacion} from '../models/cotizaciones.interface'
import {map} from 'rxjs/operators'
import { isNullOrUndefined } from 'util';


@Injectable({
  providedIn: 'root'
})
export class AceptarCotizacionService {

  constructor(private http:HttpClient) { }
  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })

  postCotizar(id:String,estado:String){
    const url = "http://localhost:1337/cotizacions/"+id;
    
   return this.http.put(url,{"estado": estado},{headers:this.headers});
  }

}
