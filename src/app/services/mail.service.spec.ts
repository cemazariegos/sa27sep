import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { MailService } from './mail.service';
import { of } from 'rxjs'; // Add import

describe('CategoriaService', () => {
  let mailService: MailService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
      ],
      providers: [
        MailService
      ],
    });

    mailService = TestBed.get(MailService);
    httpMock = TestBed.get(HttpTestingController);
  });

  describe('all', () => {
    it('Enviar correos', () => {
      const token = {
        toke:'asdasdads'
      };
      let response;
      spyOn(mailService, 'send').and.returnValue(of(token));
  
      mailService.send('test@ejemplo.com').subscribe(res => {
        response = res;
      });
  
      expect(response).not.toBeFalsy(response.token);
    });
  });
});
