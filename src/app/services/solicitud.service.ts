import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'
import {map} from 'rxjs/operators'
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class SolicitudService {

  constructor(private http:HttpClient) { }

  headers : HttpHeaders = new HttpHeaders({
    "Content-Type":"application/json"
  })
  // METODO POST PARA CREAR SOLICITUD DE SERVICIO
  post(titulo:string, descripcion:string, direccion_servicio:string, categorias:Number[]){
    const url = "http://localhost:1337/solicituds";

    return this.http.post(url,{titulo, descripcion, direccion_servicio,
      fecha: moment(new Date()).format('YYYY-MM-DD'), estado:"espera", categorias} );
  }
}
