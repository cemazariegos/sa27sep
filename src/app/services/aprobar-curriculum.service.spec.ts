import { TestBed } from '@angular/core/testing';

import { AprobarCurriculumService } from './aprobar-curriculum.service';
import { HttpClient} from '@angular/common/http'
import { HttpTestingController } from '@angular/common/http/testing'
import { proveedor } from '../models/proveedor.interface'
import { Observable } from 'rxjs';
import { Subject } from 'rxjs';


describe('AprobarCurriculumService', () => {
  /*beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AprobarCurriculumService = TestBed.get(AprobarCurriculumService);
    expect(service).toBeTruthy();
  });*/

  /*let service: AprobarCurriculumService;
  let httpMock: HttpTestingController;
  beforeEach( () => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [HttpTestingController],
      providers: [AprobarCurriculumService]
    });
    service = TestBed.get(AprobarCurriculumService);
    httpMock = TestBed.get(HttpTestingController);
  });*/
  let component: AprobarCurriculumService;
  let http: HttpClient;
  let spy: any;

  beforeEach(() => {
    component = new AprobarCurriculumService(http);
  });

  afterEach(()=>{
    component = null;
  });

  it('Prueba para recuperar un dato con la API Get',() =>{
    const dummyGet: proveedor = {
      id: 9,
      username: 'Fathom',
      email: 'diegocaballeros@gmail.com',
      provider: 'local',
      confirmed: true,
      blocked: null,
      role:{
          id: 1,
          name: 'Authenticated',
          description: 'Default role given to authenticated user.',
          type: 'authenticated',
          created_by: null,
          updated_by: null,
      },
      created_by: null,
      updated_by: null,
      created_at: '2020-08-28T05:12:55.688Z',
      updated_at: '2020-08-28T05:12:56.548Z',
      direccion: 'VillaNueva, Zona6',
      telefono: '30963906',
      tipo: 'cliente',
      nombre: 'Diego',
      apellido: 'Caballeros',
      documento: null
    };

    let proveedor$ = new Subject<proveedor[]>();
    let proveedores: proveedor[] = []; 
    proveedores.push(dummyGet);
    proveedor$.next(proveedores);
    
    spy = spyOn(component,'getProvedores').and.returnValue( proveedor$.asObservable());
    
    component.getProvedores().subscribe(res=>{
      expect(res[0].nombre).toEqual('Diego')
    })
    //spy = spyOn(component,'getFlag').and.returnValue(true);
    //expect( component.getFlag() ).toBeTruthy();
    
  })

  
  


  /* service.getProvedores().subscribe(posts=>{
      expect(posts).toEqual(dummyGet);
  });

  const request = httpMock.expectOne("http://localhost:1337/users/9");
  expect(request.request.method).toBe('GET');
  request.flush(dummyGet);
  afterEach(() => {
    httpMock.verify();
  }); */

});
