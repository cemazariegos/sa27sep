import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class AprobarCurriculumService {

  constructor(private http:HttpClient) { }

  getProvedores(){
    const url = "http://localhost:1337/users"
    return this.http.get(url)
  }

  putProveedores(id: Number){
    const url = "http://localhost:1337/users/"+id;
    return this.http.put(url, {'confirmed':true} );
  }
  getFlag(){
    return false;
  }
}
