import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {ListadoProveedorComponent} from "./listado-proveedor/listado-proveedor.component";

import { RegistroComponent } from './registro/registro.component';
import { AdministradorComponent } from './administrador/administrador.component';
import { VistaListadoClientesComponent} from './vista-listado-clientes/vista-listado-clientes.component'

import { SolicitarAyudaComponent} from './solicitar-ayuda/solicitar-ayuda.component';
import {EnviarCotizacionComponent} from './enviar-cotizacion/enviar-cotizacion.component';
import {ForgotComponent} from './forgot-password/forgot-password.component';
const routes: Routes = [
  {
    path: 'login',
    component:LoginComponent,
  },
  {
    path: 'registro',
    component:RegistroComponent
  },
  {
    path: 'vista-listado-cliente',
    component:VistaListadoClientesComponent
  },  
  {
    path: 'listado-proveedor',
    component:ListadoProveedorComponent
  },
  {
    path: 'administrador',
    component:AdministradorComponent
  },
  {
    path: 'solicitar-auida',
    component:SolicitarAyudaComponent
  },  
  {
    path: 'enviar-cotizacion/:id',
    component:EnviarCotizacionComponent
  },
  {
    path: 'forgot-password',
    component:ForgotComponent
  },
  {
    path: 'forgot-password/:t',
    component:ForgotComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
