import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {ProveedorService} from '../services/proveedor.service';
import { ListadoProveedorComponent } from './listado-proveedor.component';
import {HttpClient} from '@angular/common/http';
describe('ListadoProveedorComponent', () => {
  let component: ListadoProveedorComponent;
  let fixture: ComponentFixture<ListadoProveedorComponent>;
  let proveedorService;
  let http;
  let app: ListadoProveedorComponent;
  beforeEach(async(() => {
    http=HttpClient;
    proveedorService=   ProveedorService;
    app=new ListadoProveedorComponent(proveedorService);
  }));

  it('Esta definida el arreglo para contener las solicitudes.',async(() => {
    expect(app.servicios).toBeDefined;
  }));
  it('Si no hay solicitudes que regrese false.',async(() => {
    expect(app.setServicios(null)).toEqual(false);
  }));
  it('Se ingreso exitosamente el arreglo de solicitudes.',async(() => {
    expect(app.setServicios("id: 1,titulo: 'Reparar sillas',"+
    "descripcion: 'Se necesita que se repare siete sillas del mismo estilo, todas son de madera, "+
    "están raspadas y algunas patas quebradas.'")).toEqual(true);
  }));

  it('Cantidad correcta de solicitudes.',async(() => {
    app.servicios=["{titulo:'arreglar mesa'}","{titulo:'pintar pared'}"];
    expect(app.cantidadSolicitudes()).toEqual(2);
  }));

  //FASE 3

  it('Encontro coincidencias',async(() => {
    app.servicios=["{titulo:'Reparar mesa'}","{titulo:'pintar pared'}"];
    expect(app.buscar('mesa')).toEqual(1);
  }));
  

});
