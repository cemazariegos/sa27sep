import { Component, OnInit ,HostBinding} from '@angular/core';
import {ProveedorService} from '../services/proveedor.service';
@Component({
  selector: 'app-listado-proveedor',
  templateUrl: './listado-proveedor.component.html',
  styleUrls: ['./listado-proveedor.component.css']
})
export class ListadoProveedorComponent implements OnInit {
  texto:string="";
  servicios:any={};
  constructor(private proveedorService:ProveedorService) { }

  ngOnInit() {
    this.getServicios();
  }


  private getServicios(){
    this.proveedorService.buscarTodos().subscribe(
      res=>{
        //console.log(res);
        this.setServicios(res);
      },
      err=>{
        console.log(err);
        alert(err.message);
      }
    );
  }

  public setServicios(datos:any){
    if(datos==null){
      return false;
    }
    this.servicios=datos;
    return true;
  }
  public cantidadSolicitudes():number{
    if(this.servicios==undefined || this.servicios.length==0)
      return 0;
    return this.servicios.length;
  }

  public buscar(texto:string):number{
    if(this.cantidadSolicitudes()==0){
      return 0;
    }    
    //para guardar el listado de la consulta    
    let serviciosAux:Array<any>=new Array();
    for (const servicio of this.servicios) {
      const titulo:string =servicio.titulo.toLowerCase();
      if(titulo.includes(texto.toLowerCase()))
        serviciosAux.push(servicio);      
    }
    //actualiza el listado de servicios a mostrar
    this.servicios=serviciosAux;
    return serviciosAux.length;
  }
  

}
