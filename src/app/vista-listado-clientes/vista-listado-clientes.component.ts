import { Component, OnInit } from '@angular/core';
import { LoginService}  from '../services/login.service';
import {AceptarCotizacionService}  from '../services/aceptar-cotizacion.service';
import { Router } from '@angular/router';
import { login } from '../models/login.interface';
import {cotizacion}  from '../models/cotizaciones.interface';
import { element } from 'protractor';

@Component({
  selector: 'app-vista-listado-clientes',
  templateUrl: './vista-listado-clientes.component.html',
  styleUrls: ['./vista-listado-clientes.component.css']
})


 

export class VistaListadoClientesComponent implements OnInit {
private usuario:login;
public usu:String; 
estado:String;
id_cotizacion:String;
constructor(public listado:LoginService,public servicioCotizacion:AceptarCotizacionService ,private route:Router) { 

  this.usuario=this.listado.getUseraux();
  this.usu="";

  
  this.listado.getCotizacion(this.usuario.user.email).subscribe((res)=>{
//this.listado.getCotizacion(this.usuario).subscribe((res)=>{
   console.log(res);
   for(let re in res){
    this.Cotizaciones.push(res[re]);
   }  
  
   console.log("----------------------------------------");
   console.log(this.Cotizaciones);
   
  
   })

  }
  

  
  Cotizaciones:cotizacion[] =[];
 
  ngOnInit() {

  

}

modificarEstado(tipo:number,id_c:String){
  if(tipo===1){
    this.estado="aceptado";
    this.id_cotizacion=id_c;
   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
    
  }else if(tipo===2){
    this.estado="rechazado";
    this.id_cotizacion=id_c;
   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
  }else if(tipo===3){

    this.estado="contraofertado";
    this.id_cotizacion=id_c;
   
    console.log("-------------------: "+this.estado+ " ++++ "+this.id_cotizacion);
    this.servicioCotizacion.postCotizar(this.id_cotizacion,this.estado).subscribe((res)=>{
      window.location.reload();
    });
  }
 
  
}

mostrarNombrelogueado(){
  console.log("marcosasantosa1998@gmail.com");
     return this.usuario;
}
}
