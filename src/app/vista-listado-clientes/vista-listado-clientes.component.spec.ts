import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaListadoClientesComponent } from './vista-listado-clientes.component';
import { LoginService}  from '../services/login.service';
import {AceptarCotizacionService }  from '../services/aceptar-cotizacion.service';
import { Router } from '@angular/router';
import { ApolloTestingController, ApolloTestingModule } from 'apollo-angular/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from "@angular/router/testing";

describe('VistaListadoClientesComponent', () => {
  let component: VistaListadoClientesComponent;
  let fixture: ComponentFixture<VistaListadoClientesComponent>;
  let persona:LoginService;
  let rotues:Router;
  let cotazacion:AceptarCotizacionService;
  component = new VistaListadoClientesComponent(persona,cotazacion,rotues);


  
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VistaListadoClientesComponent] ,
      imports: [
        HttpClientTestingModule,
        ApolloTestingModule,
       FormsModule,
       RouterTestingModule
      ],
    })
    .compileComponents();
  }));
  
  beforeEach(() => {
    fixture = TestBed.createComponent(VistaListadoClientesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Acceso del usuario cliente', () => {
    expect(component).toBeTruthy();
  });
});
