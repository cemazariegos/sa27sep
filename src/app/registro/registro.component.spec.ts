import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegistroComponent } from './registro.component';
import { LoginService} from '../services/login.service'
import { UploadService } from '../services/upload.service'

describe('RegistroComponent', () => {
  let component: RegistroComponent;
  let registro: LoginService;
  let uploadService: UploadService;
  
  /*beforeEach(async(() =>{
    TestBed.configureTestingModule({
      declarations: [
        RegistroComponent
      ],
      providers: []
    }).compileComponents();

    TestBed.configureTestingModule({
      declarations: [RegistroComponent]
    })
    .createComponent(RegistroComponent);
  }));*/

  beforeEach(async(() => {
    component = new RegistroComponent(registro,uploadService)
  }));

  it('Mostrar adecuadamente el formulario de cliente', async(()=>{
    component.MostrarFormularioCliente();
    component.MostrarFormularioTrabador();
    component.MostrarFormularioCliente();
    component.MostrarFormularioTrabador();
    component.MostrarFormularioCliente();
    expect(component.clientStatus).toEqual(true);
    expect(component.workStatus).toEqual(false);
  }));

  it('Mostrar adecuadamente el formulario para un trabajador', async(()=>{
    component.MostrarFormularioTrabador();
    component.MostrarFormularioCliente();
    component.MostrarFormularioTrabador();
    component.MostrarFormularioCliente();
    component.MostrarFormularioTrabador();
    expect(component.clientStatus).toEqual(true);
    expect(component.workStatus).toEqual(true);
  }));


  it('Los archivos para subir solo deben ser PDF', async(() =>{
    expect(component.isPDF('application/pdf')).toBeTruthy()
  }));

  it('Verificar que solo se permite numeros en el campo Numero Telefonico', async(()=>{
    expect(component.validaNumero('3')).toBeTruthy();
  }));


});
