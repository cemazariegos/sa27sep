import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdministradorComponent } from './administrador.component';
import { AprobarCurriculumService } from '../services/aprobar-curriculum.service';
import { proveedor } from '../models/proveedor.interface'
import { HttpClient } from '@angular/common/http'
import { ToastService } from '../services/-toast.service';

describe('AdministradorComponent', () => {
  let component: AdministradorComponent;
  let service: AprobarCurriculumService;
  let http: HttpClient;
  let toast: ToastService;
  let spy: any;
  let auxUsers: proveedor[];

  let fixture: ComponentFixture<AdministradorComponent>;

  const dummyGet: proveedor = {
    id: 9,
    username: 'Fathom',
    email: 'diegocaballeros@gmail.com',
    provider: 'local',
    confirmed: true,
    blocked: null,
    role:{
        id: 1,
        name: 'Authenticated',
        description: 'Default role given to authenticated user.',
        type: 'authenticated',
        created_by: null,
        updated_by: null,
    },
    created_by: null,
    updated_by: null,
    created_at: '2020-08-28T05:12:55.688Z',
    updated_at: '2020-08-28T05:12:56.548Z',
    direccion: 'VillaNueva, Zona6',
    telefono: '30963906',
    tipo: 'cliente',
    nombre: 'Diego',
    apellido: 'Caballeros',
    documento: null
  };

  const dummyGet2: proveedor = {
    id: 3,
    username: 'Juanx',
    email: 'juanlopes@gmail.com',
    provider: 'local',
    confirmed: false,
    blocked: null,
    role:{
        id: 1,
        name: 'Authenticated',
        description: 'Default role given to authenticated user.',
        type: 'authenticated',
        created_by: null,
        updated_by: null,
    },
    created_by: null,
    updated_by: null,
    created_at: '2020-08-28T05:12:55.688Z',
    updated_at: '2020-08-28T05:12:56.548Z',
    direccion: 'VillaNueva, Zona6',
    telefono: '39274826',
    tipo: 'proveedor',
    nombre: 'Juan',
    apellido: 'Lopez',
    documento: 'https://bucket-reservi.s3.us-east-2.amazonaws.com/ae80302e9126a909[SO1]Clase 1.pdf'
  };

  beforeEach(async(() => {
    service = new AprobarCurriculumService(http);
    component = new AdministradorComponent(service,toast);
    auxUsers = [];
    auxUsers.push(dummyGet)
  }));

  afterEach(async(() => {
    component = null;
    spy = null;
  }));

  it('Prueba para devolver un objecto proveedor', ()=>{
    spy = spyOn(component,'getUser').and.returnValue( auxUsers);
    expect( component.getUser()[0]).toEqual(dummyGet);
  });

  it('Prueba para verificar que el usuario es un proveedor no autorizado',()=>{
    expect( component.isProviderNoAuthorized( dummyGet ) ).toBeFalsy();
  });

  it('Prueba para comprobar de que se elimino un dato de la lista user correctamente',()=>{
    
    auxUsers.push(dummyGet2);
    component.setUser(auxUsers);
    component.removeUser(dummyGet);
    let aux = component.getUser();
    expect( aux.length ).toEqual(1);
    expect( aux[0].nombre ).toEqual('Juan');
    expect( component.isProviderNoAuthorized( aux[0] ) ).toBeTruthy();
  });

  it('Prueba para comprobar el flujo del metodo authorizeProvider si es null',()=>{
    component.datosUsuario = null;
    spyOn(component,'showError')
    component.authorizeProvider()
    expect(component.showError).toHaveBeenCalled();
  });

  it('Prueba para olvidar datos de un cliente', ()=>{
    spyOn(component,'showInfo');
    
    component.deleteProvider();
    expect(component.datosUsuario).toBeNull();
    expect(component.showInfo).toHaveBeenCalled();
    expect(component.getUrlPdf()).toEqual('https://bucket-reservi.s3.us-east-2.amazonaws.com/sorry-convertido.pdf');
  });
  
});
