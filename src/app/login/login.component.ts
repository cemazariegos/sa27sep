import { Component, OnInit } from '@angular/core';
import { LoginService} from '../services/login.service'

import {login} from '../models/login.interface';
import { flatMap } from 'rxjs/operators';

import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public loginService: LoginService,private route:Router) {

   }

  ngOnInit(): void {
    //la llegada de datos
  /*  this.loginService.getUser().subscribe((res:login[])=>{
      console.log("*****************************************");
      this.credenciales= res;
    
    })*/
  }

  correo:string ="";
  contrasena:string ="";
  valido:boolean=false;
  validoaux:boolean=false;


  verificar_credenciales():boolean{
   
      console.log("Usuario:" + " "+ this.correo + " contraseña: "+ this.contrasena);

      this.loginService.postLogin(this.correo,this.contrasena).subscribe((res:login)=>{
       
      console.log("Mostrando los datos: ");
      console.log(res.user.email);
  
     
      if(res.user.email===this.correo){
        this.valido=true;
        this.correo="";
        this.contrasena="";
        
        if(res.user.tipo==="cliente"){
          this.loginService.setUser(res);
          this.route.navigate(['vista-listado-cliente']);
          
        }else{
          alert("Estamos trabajando para los demas clientes");
          //proveedor -> 
          //administrador personal ->  
        }
        }
      
      },
      error=>{
        this.validoaux=true;
        this.correo="";
        this.contrasena="";
       
      }
      
      );

     
      this.validoaux=false;
return this.valido;
    
    } 
  
   
    
   
}

